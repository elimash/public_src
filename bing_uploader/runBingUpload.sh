#!/bin/sh
startXvfb() {
   Xvfb :99 &
   sleep 10
   echo "Sleeping 10s to let Xvfb to init "
   xvfbPID=`/usr/sbin/pidof Xvfb`
}
printUsage() {
 echo "Usage:"
 echo " runBingUpload.sh [-run | -apply | -debug] [-q | -e | -X]"
 echo ""
 echo " Runs the Bing Conversions Uploader to apply our bingConversions.csv"
 echo " apply | -apply - Apply bingConversions after uploaing. If not specified program will Upload and then discard"
 echo " -debug     Use mvnDebug instead of mvn"
 echo " -q | -e | -X -    Mvn option to use. Deafult is -q"
 echo ""
 echo ""
}

if [ "$1" = "-h" ] ; then
 printUsage
 exit 0
fi
mvnPGM=mvn
if [ "$1" = "-debug" ] ; then
 mvnPGM=mvnDebug
fi
echo "Selected Maven Program mvnPGM=$mvnPGM"
minusQ="-q"
if [ "$2" = "-q" ] ; then
 minusQ="-q"
else
 minusQ="$2"
fi

export MVN_LOC=/opt/apache-maven/bin/$mvnPGM
MY_SRC_FOLDER=public_src/c_web_tests
#
# Finding out execution environment
# =================================
MY_USER=`/usr/bin/whoami`
if [ "$MY_USER" == "vagrant" ]; then
#       These are settings to run on vagrant environment
  export MY_HOME=/home/vagrant
else
#       These are settings for running on Amazon as production
  export MY_HOME=/home/ec2-user
fi
export MYPGM_LOC=$MY_HOME/$MY_SRC_FOLDER
export DISPLAY=:99
####
# We first make sure Xvfb is up in the air
####
xvfbPID=`/usr/sbin/pidof Xvfb`
if [[ -z "$xvfbPID" ]] ; then
 startXvfb
fi
echo "Xvfb is up and Running on PID=$xvfbPID"
#####################################################
# We now verify that X server is reponding to xterm
#####################################################
 xterm &
 sleep 5
xtermPID=`/usr/sbin/pidof xterm`
echo "xterm started xtermPID=$xtermPID"
if [[ -z "$xtermPID" ]] ; then
 kill $xvfbPID
 startXvfb
else
 echo "xterm opens fine, program continues"
 kill $xtermPID
fi
echo "Xvfb is up and Running on PID=$xvfbPID"
##################################################################
#  BingUploader Java program invokation
##################################################################
current_time=$(TZ=America/Los_Angeles date "+%Y-%m-%d--%H.%M.%S")
echo "Current Time : $current_time"
(cd $MYPGM_LOC; $MVN_LOC $minusQ exec:java -Dexec.mainClass="com.shadow.BingUploader" -Dexec.args="$1 $2 $3 $4 $5 $6 $7 $8 $9" )

####################################################################
# Now cleanup firefox processes if any remained
####################################################################
echo "CleanUp the mess if any"
firefoxPID=`/usr/sbin/pidof firefox`
geckoPID=`/usr/sbin/pidof geckodriver.linux`
if [[ ! -z "$firefoxPID" ]] ; then
   kill $firefoxPID
fi
if [[ ! -z "$geckoPID" ]] ; then
   kill $geckoPID
fi
