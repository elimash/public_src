import java.io.File;

/*
 * Copyright (c) 2020. Shadow Technologies Ltd. https://shadow.com
 * Author: Eli Mashiah 19/5/2020
 */

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.FileDetector;
import org.openqa.selenium.remote.RemoteWebElement;
import java.util.concurrent.TimeUnit;

/**
 * Bing Ads offers an interactive service for uploading offline conversions. We need to upload these conversions everyday
 * (which is a tedius and frequently forgotten manual process). This robot does the interavtive uploading using the batch browser
 * (selenium). We login to the Google-Ads UI and upload our CSV conversion file.
 */
public class BingUploader {
    public static final String BING_USER = "put-your-bing-user-here";
    public static final String BING_PASSWORD = "put-your-password-here";
    public static final String BING_INITIAL_ENDPOINT = "https://ads.microsoft.com/";
    public static String CSV_FILE_LOCATION = "/tmp/bingConversions.csv";    // <------ Put your CSV file here
    public static String csvFullPath = null;
    private static WebDriver driver;
    private static boolean applyUpload = false;
    public static void main(String[] args) {
        for (String arg: args) { // only if apply or -apply are specified as args we will actualy apply the upload
            if (arg.equals(("-apply")) || args.equals("apply")) {
                applyUpload = true;
            }
        }
        driver.get(BING_INITIAL_ENDPOINT);
        csvFullPath =  CSV_FILE_LOCATION;
        WebElement emailField = null;
        try {
            emailField = driver.findElement(By.id("LoginModel_Username"));
        } catch (NoSuchElementException nsex) {
            System.out.println("No loginForm found, Exiting");
            driver.close();
            System.exit(-1);
        }
        emailField.click();
        emailField.sendKeys(BING_USER);
        WebElement nextButton = driver.findElement(By.id("LoginSectionNextButton"));
        nextButton.click();
        System.out.println("We have pressed the Next field");
        WebElement passwordField = driver.findElement(By.id("i0118"));
        passwordField.sendKeys(BING_PASSWORD);
        WebElement signInButton = driver.findElement(By.id("idSIButton9"));
        signInButton.click();;
        System.out.println("We're signed in");
        WebElement conversionTracking = driver.findElement(By.id("WunderbarUetConversionsPanelTitleText"));
        conversionTracking.click();
        WebElement offlineConversions = driver.findElement(By.className("uploadConversions"));
        sleep(3000);
        offlineConversions.click();
        System.out.println("Now it's time to start uploading");
        WebElement fileInput = driver.findElement(By.id("file-input"));
        // We now create a null file detector so that no real upload will starts before we click the button
        FileDetector fileDetector = new FileDetector() {
            @Override
            public File getLocalFile(CharSequence... charSequences) {
                return null;
            }
        };
        RemoteWebElement rwd = (RemoteWebElement) fileInput;
        rwd.setFileDetector(fileDetector); // set the null file-detector
        ((JavascriptExecutor) driver).executeScript("document.getElementById('file-input').style.display='block';");
        try {
            rwd.sendKeys(csvFullPath);
        } catch(Exception x) {
            System.out.println("That's ok we expect exception msg=" + x.getMessage());
        }
        driver.manage().window().maximize();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,100)"); // scroll down for the button to be clickable
        WebElement uploadButton = driver.findElement(By.className("btn-upload"));
        uploadButton.click();
        System.out.println("File should be uploaded now");
        sleep(5000); // we wait for the upload to complete
        driver.manage().window().maximize();
        js.executeScript("window.scrollBy(0,200)");

        if (applyUpload) {
            WebElement applyButton = driver.findElement(By.className("btn-apply"));
            applyButton.click();
            System.out.println("bingConversions.csv was APPLIED");
        } else {
            WebElement discardButton = driver.findElement(By.className("btn-discard"));
            discardButton.click();
            System.out.println("bingConversions.csv was DISCARDED. Use: 'runBingUpload.sh -apply' , if you wish to apply");
        }
        driver.quit();
        System.out.println("Program Terminated");
        System.exit(0);
    }

    public static void setUp(String[] args) {
        FirefoxProfile profile = new FirefoxProfile();
        FirefoxOptions options = new FirefoxOptions();
        options.setHeadless(false);
        profile.setPreference("general.useragent.override", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0 SELENIUM");
        options.setProfile( profile);
        options.setCapability("marionette", true);

        String pathForGecko = System.getProperty("user.home") + "/public_src/bing_uploader/";
        String osName = System.getProperty("os.name");
        if (osName.contains("Linux")) {
            pathForGecko = pathForGecko + "geckodriver.linux";
        } else if (osName.contains("Mac OS") ) {
            pathForGecko = pathForGecko + "geckodriver.mac";
        } else { // if we get here we're in Windows environment
            pathForGecko = pathForGecko + "geckodriver.exe";
        }
        System.out.println("GeckoDriver=" + pathForGecko);
        System.setProperty("webdriver.gecko.driver", pathForGecko );
        FirefoxDriver firefoxDriver = null;
        try {
            firefoxDriver = new FirefoxDriver(options);
        } catch(org.openqa.selenium.SessionNotCreatedException ex) {
            System.err.println("Failed to initialise GeckoDriver. msg=" + ex.getMessage());
            System.exit(1);
        }

        if (firefoxDriver != null) {
            firefoxDriver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
            firefoxDriver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }
        driver = firefoxDriver;
    }

    public static void sleep(int timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            // we take no action
        }
    }
}
